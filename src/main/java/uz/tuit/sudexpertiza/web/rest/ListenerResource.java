package uz.tuit.sudexpertiza.web.rest;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.tuit.sudexpertiza.repository.ListenerRepository;
import uz.tuit.sudexpertiza.service.ListenerService;
import uz.tuit.sudexpertiza.service.dto.ListenerDTO;
import uz.tuit.sudexpertiza.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.tuit.sudexpertiza.domain.Listener}.
 */
@RestController
@RequestMapping("/api")
public class ListenerResource {

    private final Logger log = LoggerFactory.getLogger(ListenerResource.class);

    private static final String ENTITY_NAME = "listener";

    private final ListenerService listenerService;

    private final ListenerRepository listenerRepository;

    public ListenerResource(
        ListenerService listenerService,
        ListenerRepository listenerRepository
    ) {
        this.listenerService = listenerService;
        this.listenerRepository = listenerRepository;
    }

    /**
     * {@code POST  /listeners} : Create a new listener.
     *
     * @param listenerDTO the listenerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new listenerDTO, or with status {@code 400 (Bad Request)} if the listener has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/listeners")
    public ResponseEntity<ListenerDTO> createListener(@Valid @RequestBody ListenerDTO listenerDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save Listener : {}", listenerDTO);
        if (listenerDTO.getId() != null) {
            throw new BadRequestAlertException("A new listener cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ListenerDTO result = listenerService.save(listenerDTO);
        return ResponseEntity
            .created(new URI("/api/listeners/" + result.getId()))
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PUT  /listeners/:id} : Updates an existing listener.
     *
     * @param id the id of the listenerDTO to save.
     * @param listenerDTO the listenerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listenerDTO,
     * or with status {@code 400 (Bad Request)} if the listenerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the listenerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/listeners/{id}")
    public ResponseEntity<ListenerDTO> updateListener(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ListenerDTO listenerDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update Listener : {}, {}", id, listenerDTO);
        if (listenerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, listenerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!listenerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ListenerDTO result = listenerService.update(listenerDTO);
        return ResponseEntity
            .ok()
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PATCH  /listeners/:id} : Partial updates given fields of an existing listener, field will ignore if it is null
     *
     * @param id the id of the listenerDTO to save.
     * @param listenerDTO the listenerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listenerDTO,
     * or with status {@code 400 (Bad Request)} if the listenerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the listenerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the listenerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/listeners/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ListenerDTO> partialUpdateListener(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ListenerDTO listenerDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to partial update Listener partially : {}, {}", id, listenerDTO);
        if (listenerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, listenerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!listenerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ListenerDTO> result = listenerService.partialUpdate(listenerDTO);

        return ResponseEntity.of(result);
    }

    /**
     * {@code GET  /listeners} : get all the listeners.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of listeners in body.
     */
//    @GetMapping("/listeners")
//    public ResponseEntity<List<ListenerDTO>> getAllListeners(
//        ListenerCriteria criteria,
//        @org.springdoc.api.annotations.ParameterObject Pageable pageable
//    ) {
//        log.debug("REST request to get Listeners by criteria: {}", criteria);
//        Page<ListenerDTO> page = listenerQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /listeners/:id} : get the "id" listener.
     *
     * @param id the id of the listenerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the listenerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/listeners/{id}")
    public ResponseEntity<ListenerDTO> getListener(@PathVariable Long id) {
        log.debug("REST request to get Listener : {}", id);
        Optional<ListenerDTO> listenerDTO = listenerService.findOne(id);
        return ResponseEntity.of(listenerDTO);
    }

    /**
     * {@code DELETE  /listeners/:id} : delete the "id" listener.
     *
     * @param id the id of the listenerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/listeners/{id}")
    public ResponseEntity<Void> deleteListener(@PathVariable Long id) {
        log.debug("REST request to delete Listener : {}", id);
        listenerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(new HttpHeaders())
            .build();
    }
}
