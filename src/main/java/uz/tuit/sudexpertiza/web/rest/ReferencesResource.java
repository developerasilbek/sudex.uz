package uz.tuit.sudexpertiza.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.tuit.sudexpertiza.repository.ReferencesRepository;
import uz.tuit.sudexpertiza.service.ReferencesService;
import uz.tuit.sudexpertiza.service.dto.ReferencesDTO;
import uz.tuit.sudexpertiza.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.tuit.sudexpertiza.domain.References}.
 */
@RestController
@RequestMapping("/api")
public class ReferencesResource {

    private final Logger log = LoggerFactory.getLogger(ReferencesResource.class);

    private static final String ENTITY_NAME = "references";

    private final ReferencesService referencesService;

    private final ReferencesRepository referencesRepository;

    public ReferencesResource(
        ReferencesService referencesService,
        ReferencesRepository referencesRepository
    ) {
        this.referencesService = referencesService;
        this.referencesRepository = referencesRepository;
    }

    /**
     * {@code POST  /references} : Create a new references.
     *
     * @param referencesDTO the referencesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new referencesDTO, or with status {@code 400 (Bad Request)} if the references has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/references")
    public ResponseEntity<ReferencesDTO> createReferences(@RequestBody ReferencesDTO referencesDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save References : {}", referencesDTO);
        if (referencesDTO.getId() != null) {
            throw new BadRequestAlertException("A new references cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReferencesDTO result = referencesService.save(referencesDTO);
        return ResponseEntity
            .created(new URI("/api/references/" + result.getId()))
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PUT  /references/:id} : Updates an existing references.
     *
     * @param id the id of the referencesDTO to save.
     * @param referencesDTO the referencesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated referencesDTO,
     * or with status {@code 400 (Bad Request)} if the referencesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the referencesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/references/{id}")
    public ResponseEntity<ReferencesDTO> updateReferences(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ReferencesDTO referencesDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update References : {}, {}", id, referencesDTO);
        if (referencesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, referencesDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!referencesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ReferencesDTO result = referencesService.update(referencesDTO);
        return ResponseEntity
            .ok()
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PATCH  /references/:id} : Partial updates given fields of an existing references, field will ignore if it is null
     *
     * @param id the id of the referencesDTO to save.
     * @param referencesDTO the referencesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated referencesDTO,
     * or with status {@code 400 (Bad Request)} if the referencesDTO is not valid,
     * or with status {@code 404 (Not Found)} if the referencesDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the referencesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/references/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ReferencesDTO> partialUpdateReferences(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ReferencesDTO referencesDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to partial update References partially : {}, {}", id, referencesDTO);
        if (referencesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, referencesDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!referencesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ReferencesDTO> result = referencesService.partialUpdate(referencesDTO);

        return ResponseEntity.of(result);
    }

    /**
     * {@code GET  /references} : get all the references.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of references in body.
     */
//    @GetMapping("/references")
//    public ResponseEntity<List<ReferencesDTO>> getAllReferences(
//        ReferencesCriteria criteria,
//        @org.springdoc.api.annotations.ParameterObject Pageable pageable
//    ) {
//        log.debug("REST request to get References by criteria: {}", criteria);
//        Page<ReferencesDTO> page = referencesQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /references/:id} : get the "id" references.
     *
     * @param id the id of the referencesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the referencesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/references/{id}")
    public ResponseEntity<ReferencesDTO> getReferences(@PathVariable Long id) {
        log.debug("REST request to get References : {}", id);
        Optional<ReferencesDTO> referencesDTO = referencesService.findOne(id);
        return ResponseEntity.of(referencesDTO);
    }

    /**
     * {@code DELETE  /references/:id} : delete the "id" references.
     *
     * @param id the id of the referencesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/references/{id}")
    public ResponseEntity<Void> deleteReferences(@PathVariable Long id) {
        log.debug("REST request to delete References : {}", id);
        referencesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(new HttpHeaders())
            .build();
    }
}
