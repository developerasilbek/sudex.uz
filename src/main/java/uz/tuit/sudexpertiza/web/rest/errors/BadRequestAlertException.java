package uz.tuit.sudexpertiza.web.rest.errors;

public class BadRequestAlertException extends Exception{

    public BadRequestAlertException(String exception, String entityName, String description) {
        super(exception + "-> " + entityName+ "-> " + description);
    }

}
