package uz.tuit.sudexpertiza.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.tuit.sudexpertiza.repository.TopicTestRepository;
import uz.tuit.sudexpertiza.service.TopicTestService;
import uz.tuit.sudexpertiza.service.dto.TopicTestDTO;
import uz.tuit.sudexpertiza.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.tuit.sudexpertiza.domain.TopicTest}.
 */
@RestController
@RequestMapping("/api")
public class TopicTestResource {

    private final Logger log = LoggerFactory.getLogger(TopicTestResource.class);

    private static final String ENTITY_NAME = "topicTest";

    private final TopicTestService topicTestService;

    private final TopicTestRepository topicTestRepository;

    public TopicTestResource(
        TopicTestService topicTestService,
        TopicTestRepository topicTestRepository
    ) {
        this.topicTestService = topicTestService;
        this.topicTestRepository = topicTestRepository;
    }

    /**
     * {@code POST  /topic-tests} : Create a new topicTest.
     *
     * @param topicTestDTO the topicTestDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new topicTestDTO, or with status {@code 400 (Bad Request)} if the topicTest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/topic-tests")
    public ResponseEntity<TopicTestDTO> createTopicTest(@RequestBody TopicTestDTO topicTestDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save TopicTest : {}", topicTestDTO);
        if (topicTestDTO.getId() != null) {
            throw new BadRequestAlertException("A new topicTest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TopicTestDTO result = topicTestService.save(topicTestDTO);
        return ResponseEntity
            .created(new URI("/api/topic-tests/" + result.getId()))
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PUT  /topic-tests/:id} : Updates an existing topicTest.
     *
     * @param id the id of the topicTestDTO to save.
     * @param topicTestDTO the topicTestDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated topicTestDTO,
     * or with status {@code 400 (Bad Request)} if the topicTestDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the topicTestDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/topic-tests/{id}")
    public ResponseEntity<TopicTestDTO> updateTopicTest(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TopicTestDTO topicTestDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update TopicTest : {}, {}", id, topicTestDTO);
        if (topicTestDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, topicTestDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!topicTestRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TopicTestDTO result = topicTestService.update(topicTestDTO);
        return ResponseEntity
            .ok()
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PATCH  /topic-tests/:id} : Partial updates given fields of an existing topicTest, field will ignore if it is null
     *
     * @param id the id of the topicTestDTO to save.
     * @param topicTestDTO the topicTestDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated topicTestDTO,
     * or with status {@code 400 (Bad Request)} if the topicTestDTO is not valid,
     * or with status {@code 404 (Not Found)} if the topicTestDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the topicTestDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/topic-tests/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TopicTestDTO> partialUpdateTopicTest(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TopicTestDTO topicTestDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to partial update TopicTest partially : {}, {}", id, topicTestDTO);
        if (topicTestDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, topicTestDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!topicTestRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TopicTestDTO> result = topicTestService.partialUpdate(topicTestDTO);

        return ResponseEntity.of(result);
    }

    /**
     * {@code GET  /topic-tests} : get all the topicTests.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of topicTests in body.
     */
//    @GetMapping("/topic-tests")
//    public ResponseEntity<List<TopicTestDTO>> getAllTopicTests(
//        TopicTestCriteria criteria,
//        @org.springdoc.api.annotations.ParameterObject Pageable pageable
//    ) {
//        log.debug("REST request to get TopicTests by criteria: {}", criteria);
//        Page<TopicTestDTO> page = topicTestQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /topic-tests/:id} : get the "id" topicTest.
     *
     * @param id the id of the topicTestDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the topicTestDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/topic-tests/{id}")
    public ResponseEntity<TopicTestDTO> getTopicTest(@PathVariable Long id) {
        log.debug("REST request to get TopicTest : {}", id);
        Optional<TopicTestDTO> topicTestDTO = topicTestService.findOne(id);
        return ResponseEntity.of(topicTestDTO);
    }

    /**
     * {@code DELETE  /topic-tests/:id} : delete the "id" topicTest.
     *
     * @param id the id of the topicTestDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/topic-tests/{id}")
    public ResponseEntity<Void> deleteTopicTest(@PathVariable Long id) {
        log.debug("REST request to delete TopicTest : {}", id);
        topicTestService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(new HttpHeaders())
            .build();
    }
}
