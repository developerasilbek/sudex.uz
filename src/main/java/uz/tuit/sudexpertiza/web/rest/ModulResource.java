package uz.tuit.sudexpertiza.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.tuit.sudexpertiza.repository.ModulRepository;
import uz.tuit.sudexpertiza.service.ModulService;
import uz.tuit.sudexpertiza.service.dto.ModulDTO;
import uz.tuit.sudexpertiza.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.tuit.sudexpertiza.domain.Modul}.
 */
@RestController
@RequestMapping("/api")
public class ModulResource {

    private final Logger log = LoggerFactory.getLogger(ModulResource.class);

    private static final String ENTITY_NAME = "modul";

    private final ModulService modulService;

    private final ModulRepository modulRepository;

    public ModulResource(ModulService modulService, ModulRepository modulRepository) {
        this.modulService = modulService;
        this.modulRepository = modulRepository;
    }

    /**
     * {@code POST  /moduls} : Create a new modul.
     *
     * @param modulDTO the modulDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new modulDTO, or with status {@code 400 (Bad Request)} if the modul has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/moduls")
    public ResponseEntity<ModulDTO> createModul(@RequestBody ModulDTO modulDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save Modul : {}", modulDTO);
        if (modulDTO.getId() != null) {
            throw new BadRequestAlertException("A new modul cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModulDTO result = modulService.save(modulDTO);
        return ResponseEntity
            .created(new URI("/api/moduls/" + result.getId()))
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PUT  /moduls/:id} : Updates an existing modul.
     *
     * @param id the id of the modulDTO to save.
     * @param modulDTO the modulDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated modulDTO,
     * or with status {@code 400 (Bad Request)} if the modulDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the modulDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/moduls/{id}")
    public ResponseEntity<ModulDTO> updateModul(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ModulDTO modulDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update Modul : {}, {}", id, modulDTO);
        if (modulDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, modulDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!modulRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ModulDTO result = modulService.update(modulDTO);
        return ResponseEntity
            .ok()
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PATCH  /moduls/:id} : Partial updates given fields of an existing modul, field will ignore if it is null
     *
     * @param id the id of the modulDTO to save.
     * @param modulDTO the modulDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated modulDTO,
     * or with status {@code 400 (Bad Request)} if the modulDTO is not valid,
     * or with status {@code 404 (Not Found)} if the modulDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the modulDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/moduls/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ModulDTO> partialUpdateModul(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ModulDTO modulDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to partial update Modul partially : {}, {}", id, modulDTO);
        if (modulDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, modulDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!modulRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ModulDTO> result = modulService.partialUpdate(modulDTO);

        return ResponseEntity.of(result);
    }

    /**
     * {@code GET  /moduls} : get all the moduls.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of moduls in body.
     */
//    @GetMapping("/moduls")
//    public ResponseEntity<List<ModulDTO>> getAllModuls(
//        ModulCriteria criteria,
//        @org.springdoc.api.annotations.ParameterObject Pageable pageable
//    ) {
//        log.debug("REST request to get Moduls by criteria: {}", criteria);
//        Page<ModulDTO> page = modulQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /moduls/:id} : get the "id" modul.
     *
     * @param id the id of the modulDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the modulDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/moduls/{id}")
    public ResponseEntity<ModulDTO> getModul(@PathVariable Long id) {
        log.debug("REST request to get Modul : {}", id);
        Optional<ModulDTO> modulDTO = modulService.findOne(id);
        return ResponseEntity.of(modulDTO);
    }

    /**
     * {@code DELETE  /moduls/:id} : delete the "id" modul.
     *
     * @param id the id of the modulDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/moduls/{id}")
    public ResponseEntity<Void> deleteModul(@PathVariable Long id) {
        log.debug("REST request to delete Modul : {}", id);
        modulService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(new HttpHeaders())
            .build();
    }
}
