package uz.tuit.sudexpertiza.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.tuit.sudexpertiza.repository.GlossaryRepository;
import uz.tuit.sudexpertiza.service.GlossaryService;
import uz.tuit.sudexpertiza.service.dto.GlossaryDTO;
import uz.tuit.sudexpertiza.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.tuit.sudexpertiza.domain.Glossary}.
 */
@RestController
@RequestMapping("/api")
public class GlossaryResource {

    private final Logger log = LoggerFactory.getLogger(GlossaryResource.class);

    private static final String ENTITY_NAME = "glossary";

    private final GlossaryService glossaryService;

    private final GlossaryRepository glossaryRepository;

    public GlossaryResource(
        GlossaryService glossaryService,
        GlossaryRepository glossaryRepository
    ) {
        this.glossaryService = glossaryService;
        this.glossaryRepository = glossaryRepository;
    }

    /**
     * {@code POST  /glossaries} : Create a new glossary.
     *
     * @param glossaryDTO the glossaryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new glossaryDTO, or with status {@code 400 (Bad Request)} if the glossary has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/glossaries")
    public ResponseEntity<GlossaryDTO> createGlossary(@RequestBody GlossaryDTO glossaryDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save Glossary : {}", glossaryDTO);
        if (glossaryDTO.getId() != null) {
            throw new BadRequestAlertException("A new glossary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GlossaryDTO result = glossaryService.save(glossaryDTO);
        return ResponseEntity
            .created(new URI("/api/glossaries/" + result.getId()))
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PUT  /glossaries/:id} : Updates an existing glossary.
     *
     * @param id the id of the glossaryDTO to save.
     * @param glossaryDTO the glossaryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glossaryDTO,
     * or with status {@code 400 (Bad Request)} if the glossaryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the glossaryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/glossaries/{id}")
    public ResponseEntity<GlossaryDTO> updateGlossary(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GlossaryDTO glossaryDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update Glossary : {}, {}", id, glossaryDTO);
        if (glossaryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glossaryDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glossaryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GlossaryDTO result = glossaryService.update(glossaryDTO);
        return ResponseEntity
            .ok()
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PATCH  /glossaries/:id} : Partial updates given fields of an existing glossary, field will ignore if it is null
     *
     * @param id the id of the glossaryDTO to save.
     * @param glossaryDTO the glossaryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated glossaryDTO,
     * or with status {@code 400 (Bad Request)} if the glossaryDTO is not valid,
     * or with status {@code 404 (Not Found)} if the glossaryDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the glossaryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/glossaries/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GlossaryDTO> partialUpdateGlossary(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody GlossaryDTO glossaryDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to partial update Glossary partially : {}, {}", id, glossaryDTO);
        if (glossaryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, glossaryDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!glossaryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GlossaryDTO> result = glossaryService.partialUpdate(glossaryDTO);

        return ResponseEntity.of(result);
    }

    /**
     * {@code GET  /glossaries} : get all the glossaries.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of glossaries in body.
     */
//    @GetMapping("/glossaries")
//    public ResponseEntity<List<GlossaryDTO>> getAllGlossaries(
//        GlossaryCriteria criteria,
//        @org.springdoc.api.annotations.ParameterObject Pageable pageable
//    ) {
//        log.debug("REST request to get Glossaries by criteria: {}", criteria);
//        Page<GlossaryDTO> page = glossaryQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /glossaries/:id} : get the "id" glossary.
     *
     * @param id the id of the glossaryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the glossaryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/glossaries/{id}")
    public ResponseEntity<GlossaryDTO> getGlossary(@PathVariable Long id) {
        log.debug("REST request to get Glossary : {}", id);
        Optional<GlossaryDTO> glossaryDTO = glossaryService.findOne(id);
        return ResponseEntity.of(glossaryDTO);
    }

    /**
     * {@code DELETE  /glossaries/:id} : delete the "id" glossary.
     *
     * @param id the id of the glossaryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/glossaries/{id}")
    public ResponseEntity<Void> deleteGlossary(@PathVariable Long id) {
        log.debug("REST request to delete Glossary : {}", id);
        glossaryService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(new HttpHeaders())
            .build();
    }
}
