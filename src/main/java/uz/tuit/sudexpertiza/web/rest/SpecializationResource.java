package uz.tuit.sudexpertiza.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.tuit.sudexpertiza.repository.SpecializationRepository;
import uz.tuit.sudexpertiza.service.SpecializationService;
import uz.tuit.sudexpertiza.service.dto.SpecializationDTO;
import uz.tuit.sudexpertiza.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.tuit.sudexpertiza.domain.Specialization}.
 */
@RestController
@RequestMapping("/api")
public class SpecializationResource {

    private final Logger log = LoggerFactory.getLogger(SpecializationResource.class);

    private static final String ENTITY_NAME = "specialization";

    private final SpecializationService specializationService;

    private final SpecializationRepository specializationRepository;

    public SpecializationResource(
        SpecializationService specializationService,
        SpecializationRepository specializationRepository
    ) {
        this.specializationService = specializationService;
        this.specializationRepository = specializationRepository;
    }

    /**
     * {@code POST  /specializations} : Create a new specialization.
     *
     * @param specializationDTO the specializationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new specializationDTO, or with status {@code 400 (Bad Request)} if the specialization has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/specializations")
    public ResponseEntity<SpecializationDTO> createSpecialization(@RequestBody SpecializationDTO specializationDTO)
            throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save Specialization : {}", specializationDTO);
        if (specializationDTO.getId() != null) {
            throw new BadRequestAlertException("A new specialization cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpecializationDTO result = specializationService.save(specializationDTO);
        return ResponseEntity
            .created(new URI("/api/specializations/" + result.getId()))
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PUT  /specializations/:id} : Updates an existing specialization.
     *
     * @param id the id of the specializationDTO to save.
     * @param specializationDTO the specializationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated specializationDTO,
     * or with status {@code 400 (Bad Request)} if the specializationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the specializationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/specializations/{id}")
    public ResponseEntity<SpecializationDTO> updateSpecialization(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SpecializationDTO specializationDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update Specialization : {}, {}", id, specializationDTO);
        if (specializationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, specializationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!specializationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SpecializationDTO result = specializationService.update(specializationDTO);
        return ResponseEntity
            .ok()
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PATCH  /specializations/:id} : Partial updates given fields of an existing specialization, field will ignore if it is null
     *
     * @param id the id of the specializationDTO to save.
     * @param specializationDTO the specializationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated specializationDTO,
     * or with status {@code 400 (Bad Request)} if the specializationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the specializationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the specializationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/specializations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SpecializationDTO> partialUpdateSpecialization(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SpecializationDTO specializationDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to partial update Specialization partially : {}, {}", id, specializationDTO);
        if (specializationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, specializationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!specializationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SpecializationDTO> result = specializationService.partialUpdate(specializationDTO);

        return ResponseEntity.of(result);
    }

    /**
     * {@code GET  /specializations} : get all the specializations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of specializations in body.
     */
//    @GetMapping("/specializations")
//    public ResponseEntity<List<SpecializationDTO>> getAllSpecializations(
//        SpecializationCriteria criteria,
//        @org.springdoc.api.annotations.ParameterObject Pageable pageable
//    ) {
//        log.debug("REST request to get Specializations by criteria: {}", criteria);
//        Page<SpecializationDTO> page = specializationQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /specializations/:id} : get the "id" specialization.
     *
     * @param id the id of the specializationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the specializationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/specializations/{id}")
    public ResponseEntity<SpecializationDTO> getSpecialization(@PathVariable Long id) {
        log.debug("REST request to get Specialization : {}", id);
        Optional<SpecializationDTO> specializationDTO = specializationService.findOne(id);
        return ResponseEntity.of(specializationDTO);
    }

    /**
     * {@code DELETE  /specializations/:id} : delete the "id" specialization.
     *
     * @param id the id of the specializationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/specializations/{id}")
    public ResponseEntity<Void> deleteSpecialization(@PathVariable Long id) {
        log.debug("REST request to delete Specialization : {}", id);
        specializationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(new HttpHeaders())
            .build();
    }
}
