package uz.tuit.sudexpertiza.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.tuit.sudexpertiza.repository.LinksRepository;
import uz.tuit.sudexpertiza.service.LinksService;
import uz.tuit.sudexpertiza.service.dto.LinksDTO;
import uz.tuit.sudexpertiza.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link uz.tuit.sudexpertiza.domain.Links}.
 */
@RestController
@RequestMapping("/api")
public class LinksResource {

    private final Logger log = LoggerFactory.getLogger(LinksResource.class);

    private static final String ENTITY_NAME = "links";

     


    private final LinksService linksService;

    private final LinksRepository linksRepository;

    public LinksResource(LinksService linksService, LinksRepository linksRepository) {
        this.linksService = linksService;
        this.linksRepository = linksRepository;
    }

    /**
     * {@code POST  /links} : Create a new links.
     *
     * @param linksDTO the linksDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new linksDTO, or with status {@code 400 (Bad Request)} if the links has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/links")
    public ResponseEntity<LinksDTO> createLinks(@RequestBody LinksDTO linksDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save Links : {}", linksDTO);
        if (linksDTO.getId() != null) {
            throw new BadRequestAlertException("A new links cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LinksDTO result = linksService.save(linksDTO);
        return ResponseEntity
            .created(new URI("/api/links/" + result.getId()))
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PUT  /links/:id} : Updates an existing links.
     *
     * @param id the id of the linksDTO to save.
     * @param linksDTO the linksDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated linksDTO,
     * or with status {@code 400 (Bad Request)} if the linksDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the linksDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/links/{id}")
    public ResponseEntity<LinksDTO> updateLinks(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LinksDTO linksDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update Links : {}, {}", id, linksDTO);
        if (linksDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, linksDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!linksRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LinksDTO result = linksService.update(linksDTO);
        return ResponseEntity
            .ok()
            .headers(new HttpHeaders())
            .body(result);
    }

    /**
     * {@code PATCH  /links/:id} : Partial updates given fields of an existing links, field will ignore if it is null
     *
     * @param id the id of the linksDTO to save.
     * @param linksDTO the linksDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated linksDTO,
     * or with status {@code 400 (Bad Request)} if the linksDTO is not valid,
     * or with status {@code 404 (Not Found)} if the linksDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the linksDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/links/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<LinksDTO> partialUpdateLinks(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody LinksDTO linksDTO
    ) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to partial update Links partially : {}, {}", id, linksDTO);
        if (linksDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, linksDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!linksRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LinksDTO> result = linksService.partialUpdate(linksDTO);

        return ResponseEntity.of(result);
    }

    /**
     * {@code GET  /links} : get all the links.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of links in body.
     */
//    @GetMapping("/links")
//    public ResponseEntity<List<LinksDTO>> getAllLinks(
//        LinksCriteria criteria,
//        @org.springdoc.api.annotations.ParameterObject Pageable pageable
//    ) {
//        log.debug("REST request to get Links by criteria: {}", criteria);
//        Page<LinksDTO> page = linksQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /links/:id} : get the "id" links.
     *
     * @param id the id of the linksDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the linksDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/links/{id}")
    public ResponseEntity<LinksDTO> getLinks(@PathVariable Long id) {
        log.debug("REST request to get Links : {}", id);
        Optional<LinksDTO> linksDTO = linksService.findOne(id);
        return ResponseEntity.of(linksDTO);
    }

    /**
     * {@code DELETE  /links/:id} : delete the "id" links.
     *
     * @param id the id of the linksDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/links/{id}")
    public ResponseEntity<Void> deleteLinks(@PathVariable Long id) {
        log.debug("REST request to delete Links : {}", id);
        linksService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(new HttpHeaders())
            .build();
    }
}
