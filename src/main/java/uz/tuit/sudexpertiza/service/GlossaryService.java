package uz.tuit.sudexpertiza.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.tuit.sudexpertiza.domain.Glossary;
import uz.tuit.sudexpertiza.repository.GlossaryRepository;
import uz.tuit.sudexpertiza.service.dto.GlossaryDTO;
import uz.tuit.sudexpertiza.service.mapper.GlossaryMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Glossary}.
 */
@Service
@Transactional
public class GlossaryService {

    private final Logger log = LoggerFactory.getLogger(GlossaryService.class);

    private final GlossaryRepository glossaryRepository;

    private final GlossaryMapper glossaryMapper;

    public GlossaryService(GlossaryRepository glossaryRepository, GlossaryMapper glossaryMapper) {
        this.glossaryRepository = glossaryRepository;
        this.glossaryMapper = glossaryMapper;
    }

    /**
     * Save a glossary.
     *
     * @param glossaryDTO the entity to save.
     * @return the persisted entity.
     */
    public GlossaryDTO save(GlossaryDTO glossaryDTO) {
        log.debug("Request to save Glossary : {}", glossaryDTO);
        Glossary glossary = glossaryMapper.toEntity(glossaryDTO);
        glossary = glossaryRepository.save(glossary);
        return glossaryMapper.toDto(glossary);
    }

    /**
     * Update a glossary.
     *
     * @param glossaryDTO the entity to save.
     * @return the persisted entity.
     */
    public GlossaryDTO update(GlossaryDTO glossaryDTO) {
        log.debug("Request to update Glossary : {}", glossaryDTO);
        Glossary glossary = glossaryMapper.toEntity(glossaryDTO);
        glossary = glossaryRepository.save(glossary);
        return glossaryMapper.toDto(glossary);
    }

    /**
     * Partially update a glossary.
     *
     * @param glossaryDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<GlossaryDTO> partialUpdate(GlossaryDTO glossaryDTO) {
        log.debug("Request to partially update Glossary : {}", glossaryDTO);

        return glossaryRepository
            .findById(glossaryDTO.getId())
            .map(existingGlossary -> {
                glossaryMapper.partialUpdate(existingGlossary, glossaryDTO);

                return existingGlossary;
            })
            .map(glossaryRepository::save)
            .map(glossaryMapper::toDto);
    }

    /**
     * Get all the glossaries.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<GlossaryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Glossaries");
        return glossaryRepository.findAll(pageable).map(glossaryMapper::toDto);
    }

    /**
     * Get one glossary by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<GlossaryDTO> findOne(Long id) {
        log.debug("Request to get Glossary : {}", id);
        return glossaryRepository.findById(id).map(glossaryMapper::toDto);
    }

    /**
     * Delete the glossary by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Glossary : {}", id);
        glossaryRepository.deleteById(id);
    }
}
