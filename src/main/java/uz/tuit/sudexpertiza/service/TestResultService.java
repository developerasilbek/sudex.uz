package uz.tuit.sudexpertiza.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.tuit.sudexpertiza.domain.TestResult;
import uz.tuit.sudexpertiza.repository.TestResultRepository;
import uz.tuit.sudexpertiza.service.dto.TestResultDTO;
import uz.tuit.sudexpertiza.service.mapper.TestResultMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TestResult}.
 */
@Service
@Transactional
public class TestResultService {

    private final Logger log = LoggerFactory.getLogger(TestResultService.class);

    private final TestResultRepository testResultRepository;

    private final TestResultMapper testResultMapper;

    public TestResultService(TestResultRepository testResultRepository, TestResultMapper testResultMapper) {
        this.testResultRepository = testResultRepository;
        this.testResultMapper = testResultMapper;
    }

    /**
     * Save a testResult.
     *
     * @param testResultDTO the entity to save.
     * @return the persisted entity.
     */
    public TestResultDTO save(TestResultDTO testResultDTO) {
        log.debug("Request to save TestResult : {}", testResultDTO);
        TestResult testResult = testResultMapper.toEntity(testResultDTO);
        testResult = testResultRepository.save(testResult);
        return testResultMapper.toDto(testResult);
    }

    /**
     * Update a testResult.
     *
     * @param testResultDTO the entity to save.
     * @return the persisted entity.
     */
    public TestResultDTO update(TestResultDTO testResultDTO) {
        log.debug("Request to update TestResult : {}", testResultDTO);
        TestResult testResult = testResultMapper.toEntity(testResultDTO);
        testResult = testResultRepository.save(testResult);
        return testResultMapper.toDto(testResult);
    }

    /**
     * Partially update a testResult.
     *
     * @param testResultDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<TestResultDTO> partialUpdate(TestResultDTO testResultDTO) {
        log.debug("Request to partially update TestResult : {}", testResultDTO);

        return testResultRepository
            .findById(testResultDTO.getId())
            .map(existingTestResult -> {
                testResultMapper.partialUpdate(existingTestResult, testResultDTO);

                return existingTestResult;
            })
            .map(testResultRepository::save)
            .map(testResultMapper::toDto);
    }

    /**
     * Get all the testResults.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TestResultDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TestResults");
        return testResultRepository.findAll(pageable).map(testResultMapper::toDto);
    }

    /**
     * Get one testResult by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TestResultDTO> findOne(Long id) {
        log.debug("Request to get TestResult : {}", id);
        return testResultRepository.findById(id).map(testResultMapper::toDto);
    }

    /**
     * Delete the testResult by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TestResult : {}", id);
        testResultRepository.deleteById(id);
    }
}
