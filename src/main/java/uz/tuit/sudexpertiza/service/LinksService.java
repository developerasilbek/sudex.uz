package uz.tuit.sudexpertiza.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.tuit.sudexpertiza.domain.Links;
import uz.tuit.sudexpertiza.repository.LinksRepository;
import uz.tuit.sudexpertiza.service.dto.LinksDTO;
import uz.tuit.sudexpertiza.service.mapper.LinksMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Links}.
 */
@Service
@Transactional
public class LinksService {

    private final Logger log = LoggerFactory.getLogger(LinksService.class);

    private final LinksRepository linksRepository;

    private final LinksMapper linksMapper;

    public LinksService(LinksRepository linksRepository, LinksMapper linksMapper) {
        this.linksRepository = linksRepository;
        this.linksMapper = linksMapper;
    }

    /**
     * Save a links.
     *
     * @param linksDTO the entity to save.
     * @return the persisted entity.
     */
    public LinksDTO save(LinksDTO linksDTO) {
        log.debug("Request to save Links : {}", linksDTO);
        Links links = linksMapper.toEntity(linksDTO);
        links = linksRepository.save(links);
        return linksMapper.toDto(links);
    }

    /**
     * Update a links.
     *
     * @param linksDTO the entity to save.
     * @return the persisted entity.
     */
    public LinksDTO update(LinksDTO linksDTO) {
        log.debug("Request to update Links : {}", linksDTO);
        Links links = linksMapper.toEntity(linksDTO);
        links = linksRepository.save(links);
        return linksMapper.toDto(links);
    }

    /**
     * Partially update a links.
     *
     * @param linksDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<LinksDTO> partialUpdate(LinksDTO linksDTO) {
        log.debug("Request to partially update Links : {}", linksDTO);

        return linksRepository
            .findById(linksDTO.getId())
            .map(existingLinks -> {
                linksMapper.partialUpdate(existingLinks, linksDTO);

                return existingLinks;
            })
            .map(linksRepository::save)
            .map(linksMapper::toDto);
    }

    /**
     * Get all the links.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LinksDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Links");
        return linksRepository.findAll(pageable).map(linksMapper::toDto);
    }

    /**
     * Get one links by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LinksDTO> findOne(Long id) {
        log.debug("Request to get Links : {}", id);
        return linksRepository.findById(id).map(linksMapper::toDto);
    }

    /**
     * Delete the links by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Links : {}", id);
        linksRepository.deleteById(id);
    }
}
