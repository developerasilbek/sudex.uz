package uz.tuit.sudexpertiza.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.tuit.sudexpertiza.domain.Listener;
import uz.tuit.sudexpertiza.repository.ListenerRepository;
import uz.tuit.sudexpertiza.service.dto.ListenerDTO;
import uz.tuit.sudexpertiza.service.mapper.ListenerMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Listener}.
 */
@Service
@Transactional
public class ListenerService {

    private final Logger log = LoggerFactory.getLogger(ListenerService.class);

    private final ListenerRepository listenerRepository;

    private final ListenerMapper listenerMapper;

    public ListenerService(ListenerRepository listenerRepository, ListenerMapper listenerMapper) {
        this.listenerRepository = listenerRepository;
        this.listenerMapper = listenerMapper;
    }

    /**
     * Save a listener.
     *
     * @param listenerDTO the entity to save.
     * @return the persisted entity.
     */
    public ListenerDTO save(ListenerDTO listenerDTO) {
        log.debug("Request to save Listener : {}", listenerDTO);
        Listener listener = listenerMapper.toEntity(listenerDTO);
        listener = listenerRepository.save(listener);
        return listenerMapper.toDto(listener);
    }

    /**
     * Update a listener.
     *
     * @param listenerDTO the entity to save.
     * @return the persisted entity.
     */
    public ListenerDTO update(ListenerDTO listenerDTO) {
        log.debug("Request to update Listener : {}", listenerDTO);
        Listener listener = listenerMapper.toEntity(listenerDTO);
        listener = listenerRepository.save(listener);
        return listenerMapper.toDto(listener);
    }

    /**
     * Partially update a listener.
     *
     * @param listenerDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ListenerDTO> partialUpdate(ListenerDTO listenerDTO) {
        log.debug("Request to partially update Listener : {}", listenerDTO);

        return listenerRepository
            .findById(listenerDTO.getId())
            .map(existingListener -> {
                listenerMapper.partialUpdate(existingListener, listenerDTO);

                return existingListener;
            })
            .map(listenerRepository::save)
            .map(listenerMapper::toDto);
    }

    /**
     * Get all the listeners.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ListenerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Listeners");
        return listenerRepository.findAll(pageable).map(listenerMapper::toDto);
    }

    /**
     * Get one listener by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ListenerDTO> findOne(Long id) {
        log.debug("Request to get Listener : {}", id);
        return listenerRepository.findById(id).map(listenerMapper::toDto);
    }

    /**
     * Delete the listener by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Listener : {}", id);
        listenerRepository.deleteById(id);
    }
}
