package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.Topic} entity.
 */
@Schema(description = "Topic")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TopicDTO implements Serializable {

    private Long id;

    /**
     * name
     */
    @Schema(description = "name")
    private String name;

    /**
     * priority of topic
     */
    @Schema(description = "priority of topic")
    private Integer priority;

    /**
     * hour of topic
     */
    @Schema(description = "hour of topic")
    private Integer hour;

    private ModulDTO modul;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public ModulDTO getModul() {
        return modul;
    }

    public void setModul(ModulDTO modul) {
        this.modul = modul;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TopicDTO)) {
            return false;
        }

        TopicDTO topicDTO = (TopicDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, topicDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TopicDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", priority=" + getPriority() +
            ", hour=" + getHour() +
            ", modul=" + getModul() +
            "}";
    }
}
