package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.Glossary} entity.
 */
@Schema(description = "Glossary of topic")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GlossaryDTO implements Serializable {

    private Long id;

    /**
     * name
     */
    @Schema(description = "name")
    private String name;

    private AssessmentDTO assessment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AssessmentDTO getAssessment() {
        return assessment;
    }

    public void setAssessment(AssessmentDTO assessment) {
        this.assessment = assessment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GlossaryDTO)) {
            return false;
        }

        GlossaryDTO glossaryDTO = (GlossaryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, glossaryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GlossaryDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", assessment=" + getAssessment() +
            "}";
    }
}
