package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import uz.tuit.sudexpertiza.domain.enumeration.Centre;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.Specialization} entity.
 */
@Schema(description = "Specialization")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SpecializationDTO implements Serializable {

    private Long id;

    /**
     * name
     */
    @Schema(description = "name")
    private String name;

    /**
     * centre
     */
    @Schema(description = "centre")
    private Centre centre;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Centre getCentre() {
        return centre;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpecializationDTO)) {
            return false;
        }

        SpecializationDTO specializationDTO = (SpecializationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, specializationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SpecializationDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", centre='" + getCentre() + "'" +
            "}";
    }
}
