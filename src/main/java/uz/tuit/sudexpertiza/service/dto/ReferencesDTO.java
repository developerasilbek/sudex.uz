package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.References} entity.
 */
@Schema(description = "References of topic")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ReferencesDTO implements Serializable {

    private Long id;

    /**
     * name of reference
     */
    @Schema(description = "name of reference")
    private String name;

    private AssessmentDTO assessment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AssessmentDTO getAssessment() {
        return assessment;
    }

    public void setAssessment(AssessmentDTO assessment) {
        this.assessment = assessment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReferencesDTO)) {
            return false;
        }

        ReferencesDTO referencesDTO = (ReferencesDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, referencesDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ReferencesDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", assessment=" + getAssessment() +
            "}";
    }
}
