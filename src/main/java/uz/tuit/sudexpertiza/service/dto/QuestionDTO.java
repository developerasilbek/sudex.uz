package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import uz.tuit.sudexpertiza.domain.enumeration.QuestionType;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.Question} entity.
 */
@Schema(description = "Question of test")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class QuestionDTO implements Serializable {

    private Long id;

    /**
     * name of question
     */
    @Schema(description = "name of question")
    private String name;

    /**
     * required score
     */
    @Schema(description = "required score")
    private Integer requiredScore;

    /**
     * question type
     */
    @Schema(description = "question type")
    private QuestionType type;

    private TopicTestDTO test;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRequiredScore() {
        return requiredScore;
    }

    public void setRequiredScore(Integer requiredScore) {
        this.requiredScore = requiredScore;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public TopicTestDTO getTest() {
        return test;
    }

    public void setTest(TopicTestDTO test) {
        this.test = test;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuestionDTO)) {
            return false;
        }

        QuestionDTO questionDTO = (QuestionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, questionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuestionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", requiredScore=" + getRequiredScore() +
            ", type='" + getType() + "'" +
            ", test=" + getTest() +
            "}";
    }
}
