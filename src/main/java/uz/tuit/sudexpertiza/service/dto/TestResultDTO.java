package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.TestResult} entity.
 */
@Schema(description = "Result of Test")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TestResultDTO implements Serializable {

    private Long id;

    /**
     * started at
     */
    @Schema(description = "started at")
    private Instant startedAt;

    /**
     * finished at
     */
    @Schema(description = "finished at")
    private Instant finishedAt;

    /**
     * score
     */
    @Schema(description = "score")
    private Integer score;

    /**
     * number of correct answers
     */
    @Schema(description = "number of correct answers")
    private Integer correctAnswers;

    private TopicTestDTO test;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Instant startedAt) {
        this.startedAt = startedAt;
    }

    public Instant getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Instant finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(Integer correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public TopicTestDTO getTest() {
        return test;
    }

    public void setTest(TopicTestDTO test) {
        this.test = test;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestResultDTO)) {
            return false;
        }

        TestResultDTO testResultDTO = (TestResultDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, testResultDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TestResultDTO{" +
            "id=" + getId() +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            ", score=" + getScore() +
            ", correctAnswers=" + getCorrectAnswers() +
            ", test=" + getTest() +
            "}";
    }
}
