package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.TopicTest} entity.
 */
@Schema(description = "Test of topic")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TopicTestDTO implements Serializable {

    private Long id;

    /**
     * name of test
     */
    @Schema(description = "name of test")
    private String name;

    private AssessmentDTO assessment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AssessmentDTO getAssessment() {
        return assessment;
    }

    public void setAssessment(AssessmentDTO assessment) {
        this.assessment = assessment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TopicTestDTO)) {
            return false;
        }

        TopicTestDTO topicTestDTO = (TopicTestDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, topicTestDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TopicTestDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", assessment=" + getAssessment() +
            "}";
    }
}
