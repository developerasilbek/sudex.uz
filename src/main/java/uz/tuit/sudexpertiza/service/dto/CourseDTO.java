package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import uz.tuit.sudexpertiza.domain.enumeration.Centre;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.Course} entity.
 */
@Schema(description = "Course")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CourseDTO implements Serializable {

    private Long id;

    /**
     * name
     */
    @Schema(description = "name")
    private String name;

    /**
     * centre
     */
    @Schema(description = "centre")
    private Centre centre;

    /**
     * language
     */
    @Schema(description = "language")
    private String language;

    /**
     * teaching hour
     */
    @Schema(description = "teaching hour")
    private Integer teachingHour;

    /**
     * started at
     */
    @Schema(description = "started at")
    private Instant startedAt;

    /**
     * finished at
     */
    @Schema(description = "finished at")
    private Instant finishedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Centre getCentre() {
        return centre;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getTeachingHour() {
        return teachingHour;
    }

    public void setTeachingHour(Integer teachingHour) {
        this.teachingHour = teachingHour;
    }

    public Instant getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Instant startedAt) {
        this.startedAt = startedAt;
    }

    public Instant getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Instant finishedAt) {
        this.finishedAt = finishedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CourseDTO)) {
            return false;
        }

        CourseDTO courseDTO = (CourseDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, courseDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CourseDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", centre='" + getCentre() + "'" +
            ", language='" + getLanguage() + "'" +
            ", teachingHour=" + getTeachingHour() +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            "}";
    }
}
