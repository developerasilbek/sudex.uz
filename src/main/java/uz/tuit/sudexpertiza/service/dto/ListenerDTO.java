package uz.tuit.sudexpertiza.service.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import uz.tuit.sudexpertiza.domain.enumeration.ConditionType;
import uz.tuit.sudexpertiza.domain.enumeration.StudyType;

import jakarta.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link uz.tuit.sudexpertiza.domain.Listener} entity.
 */
@Schema(description = "Listener")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ListenerDTO implements Serializable {

    private Long id;

    /**
     * phone number
     */
    @NotNull
    @Size(max = 15)
    @Schema(description = "phone number", required = true)
    private String phone;

    /**
     * region
     */
    @Schema(description = "region")
    private String region;

    /**
     * workplace
     */
    @Schema(description = "workplace")
    private String workplace;

    /**
     * Date of birth
     */
    @Schema(description = "Date of birth")
    private String dateOfBirth;

    /**
     * condition of listener
     */
    @Schema(description = "condition of listener")
    private ConditionType condition;

    /**
     * study type
     */
    @Schema(description = "study type")
    private StudyType type;

    private UserDTO user;

    private AttachmentDTO contract;

    private CourseDTO course;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public ConditionType getCondition() {
        return condition;
    }

    public void setCondition(ConditionType condition) {
        this.condition = condition;
    }

    public StudyType getType() {
        return type;
    }

    public void setType(StudyType type) {
        this.type = type;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public AttachmentDTO getContract() {
        return contract;
    }

    public void setContract(AttachmentDTO contract) {
        this.contract = contract;
    }

    public CourseDTO getCourse() {
        return course;
    }

    public void setCourse(CourseDTO course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ListenerDTO)) {
            return false;
        }

        ListenerDTO listenerDTO = (ListenerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, listenerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ListenerDTO{" +
            "id=" + getId() +
            ", phone='" + getPhone() + "'" +
            ", region='" + getRegion() + "'" +
            ", workplace='" + getWorkplace() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", condition='" + getCondition() + "'" +
            ", type='" + getType() + "'" +
            ", user=" + getUser() +
            ", contract=" + getContract() +
            ", course=" + getCourse() +
            "}";
    }
}
