package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Specialization;
import uz.tuit.sudexpertiza.service.dto.SpecializationDTO;

/**
 * Mapper for the entity {@link Specialization} and its DTO {@link SpecializationDTO}.
 */
@Mapper(componentModel = "spring")
public interface SpecializationMapper extends EntityMapper<SpecializationDTO, Specialization> {}
