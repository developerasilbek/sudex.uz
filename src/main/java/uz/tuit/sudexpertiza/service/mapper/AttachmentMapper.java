package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Attachment;
import uz.tuit.sudexpertiza.domain.Topic;
import uz.tuit.sudexpertiza.service.dto.AttachmentDTO;
import uz.tuit.sudexpertiza.service.dto.TopicDTO;

/**
 * Mapper for the entity {@link Attachment} and its DTO {@link AttachmentDTO}.
 */
@Mapper(componentModel = "spring")
public interface AttachmentMapper extends EntityMapper<AttachmentDTO, Attachment> {
    @Mapping(target = "topic", source = "topic", qualifiedByName = "topicId")
    AttachmentDTO toDto(Attachment s);

    @Named("topicId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TopicDTO toDtoTopicId(Topic topic);
}
