package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Course;
import uz.tuit.sudexpertiza.service.dto.CourseDTO;

/**
 * Mapper for the entity {@link Course} and its DTO {@link CourseDTO}.
 */
@Mapper(componentModel = "spring")
public interface CourseMapper extends EntityMapper<CourseDTO, Course> {}
