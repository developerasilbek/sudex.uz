package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Assessment;
import uz.tuit.sudexpertiza.domain.References;
import uz.tuit.sudexpertiza.service.dto.AssessmentDTO;
import uz.tuit.sudexpertiza.service.dto.ReferencesDTO;

/**
 * Mapper for the entity {@link References} and its DTO {@link ReferencesDTO}.
 */
@Mapper(componentModel = "spring")
public interface ReferencesMapper extends EntityMapper<ReferencesDTO, References> {
    @Mapping(target = "assessment", source = "assessment", qualifiedByName = "assessmentId")
    ReferencesDTO toDto(References s);

    @Named("assessmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AssessmentDTO toDtoAssessmentId(Assessment assessment);
}
