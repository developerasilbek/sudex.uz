package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Assessment;
import uz.tuit.sudexpertiza.domain.TopicTest;
import uz.tuit.sudexpertiza.service.dto.AssessmentDTO;
import uz.tuit.sudexpertiza.service.dto.TopicTestDTO;

/**
 * Mapper for the entity {@link TopicTest} and its DTO {@link TopicTestDTO}.
 */
@Mapper(componentModel = "spring")
public interface TopicTestMapper extends EntityMapper<TopicTestDTO, TopicTest> {
    @Mapping(target = "assessment", source = "assessment", qualifiedByName = "assessmentId")
    TopicTestDTO toDto(TopicTest s);

    @Named("assessmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AssessmentDTO toDtoAssessmentId(Assessment assessment);
}
