package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Assessment;
import uz.tuit.sudexpertiza.domain.Glossary;
import uz.tuit.sudexpertiza.service.dto.AssessmentDTO;
import uz.tuit.sudexpertiza.service.dto.GlossaryDTO;

/**
 * Mapper for the entity {@link Glossary} and its DTO {@link GlossaryDTO}.
 */
@Mapper(componentModel = "spring")
public interface GlossaryMapper extends EntityMapper<GlossaryDTO, Glossary> {
    @Mapping(target = "assessment", source = "assessment", qualifiedByName = "assessmentId")
    GlossaryDTO toDto(Glossary s);

    @Named("assessmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AssessmentDTO toDtoAssessmentId(Assessment assessment);
}
