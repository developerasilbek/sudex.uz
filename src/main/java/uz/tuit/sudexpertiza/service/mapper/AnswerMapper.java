package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Answer;
import uz.tuit.sudexpertiza.domain.Question;
import uz.tuit.sudexpertiza.service.dto.AnswerDTO;
import uz.tuit.sudexpertiza.service.dto.QuestionDTO;

/**
 * Mapper for the entity {@link Answer} and its DTO {@link AnswerDTO}.
 */
@Mapper(componentModel = "spring")
public interface AnswerMapper extends EntityMapper<AnswerDTO, Answer> {
    @Mapping(target = "question", source = "question", qualifiedByName = "questionName")
    AnswerDTO toDto(Answer s);

    @Named("questionName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    QuestionDTO toDtoQuestionName(Question question);
}
