package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Block;
import uz.tuit.sudexpertiza.domain.Modul;
import uz.tuit.sudexpertiza.service.dto.BlockDTO;
import uz.tuit.sudexpertiza.service.dto.ModulDTO;

/**
 * Mapper for the entity {@link Modul} and its DTO {@link ModulDTO}.
 */
@Mapper(componentModel = "spring")
public interface ModulMapper extends EntityMapper<ModulDTO, Modul> {
    @Mapping(target = "block", source = "block", qualifiedByName = "blockId")
    ModulDTO toDto(Modul s);

    @Named("blockId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    BlockDTO toDtoBlockId(Block block);
}
