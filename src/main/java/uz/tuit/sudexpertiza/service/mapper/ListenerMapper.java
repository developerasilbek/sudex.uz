package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Attachment;
import uz.tuit.sudexpertiza.domain.Course;
import uz.tuit.sudexpertiza.domain.Listener;
import uz.tuit.sudexpertiza.domain.User;
import uz.tuit.sudexpertiza.service.dto.AttachmentDTO;
import uz.tuit.sudexpertiza.service.dto.CourseDTO;
import uz.tuit.sudexpertiza.service.dto.ListenerDTO;
import uz.tuit.sudexpertiza.service.dto.UserDTO;

/**
 * Mapper for the entity {@link Listener} and its DTO {@link ListenerDTO}.
 */
@Mapper(componentModel = "spring")
public interface ListenerMapper extends EntityMapper<ListenerDTO, Listener> {
    @Mapping(target = "user", source = "user", qualifiedByName = "userId")
    @Mapping(target = "contract", source = "contract", qualifiedByName = "attachmentId")
    @Mapping(target = "course", source = "course", qualifiedByName = "courseId")
    ListenerDTO toDto(Listener s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);

    @Named("attachmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AttachmentDTO toDtoAttachmentId(Attachment attachment);

    @Named("courseId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourseDTO toDtoCourseId(Course course);
}
