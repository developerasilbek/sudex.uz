package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Modul;
import uz.tuit.sudexpertiza.domain.Topic;
import uz.tuit.sudexpertiza.service.dto.ModulDTO;
import uz.tuit.sudexpertiza.service.dto.TopicDTO;

/**
 * Mapper for the entity {@link Topic} and its DTO {@link TopicDTO}.
 */
@Mapper(componentModel = "spring")
public interface TopicMapper extends EntityMapper<TopicDTO, Topic> {
    @Mapping(target = "modul", source = "modul", qualifiedByName = "modulName")
    TopicDTO toDto(Topic s);

    @Named("modulName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    ModulDTO toDtoModulName(Modul modul);
}
