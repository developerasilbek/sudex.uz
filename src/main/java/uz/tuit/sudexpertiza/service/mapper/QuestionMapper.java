package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Question;
import uz.tuit.sudexpertiza.domain.TopicTest;
import uz.tuit.sudexpertiza.service.dto.QuestionDTO;
import uz.tuit.sudexpertiza.service.dto.TopicTestDTO;

/**
 * Mapper for the entity {@link Question} and its DTO {@link QuestionDTO}.
 */
@Mapper(componentModel = "spring")
public interface QuestionMapper extends EntityMapper<QuestionDTO, Question> {
    @Mapping(target = "test", source = "test", qualifiedByName = "topicTestName")
    QuestionDTO toDto(Question s);

    @Named("topicTestName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    TopicTestDTO toDtoTopicTestName(TopicTest topicTest);
}
