package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.TestResult;
import uz.tuit.sudexpertiza.domain.TopicTest;
import uz.tuit.sudexpertiza.service.dto.TestResultDTO;
import uz.tuit.sudexpertiza.service.dto.TopicTestDTO;

/**
 * Mapper for the entity {@link TestResult} and its DTO {@link TestResultDTO}.
 */
@Mapper(componentModel = "spring")
public interface TestResultMapper extends EntityMapper<TestResultDTO, TestResult> {
    @Mapping(target = "test", source = "test", qualifiedByName = "topicTestId")
    TestResultDTO toDto(TestResult s);

    @Named("topicTestId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TopicTestDTO toDtoTopicTestId(TopicTest topicTest);
}
