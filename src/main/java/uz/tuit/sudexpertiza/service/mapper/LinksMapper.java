package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Assessment;
import uz.tuit.sudexpertiza.domain.Links;
import uz.tuit.sudexpertiza.service.dto.AssessmentDTO;
import uz.tuit.sudexpertiza.service.dto.LinksDTO;

/**
 * Mapper for the entity {@link Links} and its DTO {@link LinksDTO}.
 */
@Mapper(componentModel = "spring")
public interface LinksMapper extends EntityMapper<LinksDTO, Links> {
    @Mapping(target = "assessment", source = "assessment", qualifiedByName = "assessmentId")
    LinksDTO toDto(Links s);

    @Named("assessmentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AssessmentDTO toDtoAssessmentId(Assessment assessment);
}
