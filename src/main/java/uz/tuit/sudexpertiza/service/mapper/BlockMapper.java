package uz.tuit.sudexpertiza.service.mapper;

import org.mapstruct.*;
import uz.tuit.sudexpertiza.domain.Block;
import uz.tuit.sudexpertiza.domain.Course;
import uz.tuit.sudexpertiza.domain.Specialization;
import uz.tuit.sudexpertiza.service.dto.BlockDTO;
import uz.tuit.sudexpertiza.service.dto.CourseDTO;
import uz.tuit.sudexpertiza.service.dto.SpecializationDTO;

/**
 * Mapper for the entity {@link Block} and its DTO {@link BlockDTO}.
 */
@Mapper(componentModel = "spring")
public interface BlockMapper extends EntityMapper<BlockDTO, Block> {
    @Mapping(target = "course", source = "course", qualifiedByName = "courseId")
    @Mapping(target = "specialization", source = "specialization", qualifiedByName = "specializationId")
    BlockDTO toDto(Block s);

    @Named("courseId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourseDTO toDtoCourseId(Course course);

    @Named("specializationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SpecializationDTO toDtoSpecializationId(Specialization specialization);
}
