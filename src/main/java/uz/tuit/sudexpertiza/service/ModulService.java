package uz.tuit.sudexpertiza.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.tuit.sudexpertiza.domain.Modul;
import uz.tuit.sudexpertiza.repository.ModulRepository;
import uz.tuit.sudexpertiza.service.dto.ModulDTO;
import uz.tuit.sudexpertiza.service.mapper.ModulMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Modul}.
 */
@Service
@Transactional
public class ModulService {

    private final Logger log = LoggerFactory.getLogger(ModulService.class);

    private final ModulRepository modulRepository;

    private final ModulMapper modulMapper;

    public ModulService(ModulRepository modulRepository, ModulMapper modulMapper) {
        this.modulRepository = modulRepository;
        this.modulMapper = modulMapper;
    }

    /**
     * Save a modul.
     *
     * @param modulDTO the entity to save.
     * @return the persisted entity.
     */
    public ModulDTO save(ModulDTO modulDTO) {
        log.debug("Request to save Modul : {}", modulDTO);
        Modul modul = modulMapper.toEntity(modulDTO);
        modul = modulRepository.save(modul);
        return modulMapper.toDto(modul);
    }

    /**
     * Update a modul.
     *
     * @param modulDTO the entity to save.
     * @return the persisted entity.
     */
    public ModulDTO update(ModulDTO modulDTO) {
        log.debug("Request to update Modul : {}", modulDTO);
        Modul modul = modulMapper.toEntity(modulDTO);
        modul = modulRepository.save(modul);
        return modulMapper.toDto(modul);
    }

    /**
     * Partially update a modul.
     *
     * @param modulDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ModulDTO> partialUpdate(ModulDTO modulDTO) {
        log.debug("Request to partially update Modul : {}", modulDTO);

        return modulRepository
            .findById(modulDTO.getId())
            .map(existingModul -> {
                modulMapper.partialUpdate(existingModul, modulDTO);

                return existingModul;
            })
            .map(modulRepository::save)
            .map(modulMapper::toDto);
    }

    /**
     * Get all the moduls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ModulDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Moduls");
        return modulRepository.findAll(pageable).map(modulMapper::toDto);
    }

    /**
     * Get one modul by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ModulDTO> findOne(Long id) {
        log.debug("Request to get Modul : {}", id);
        return modulRepository.findById(id).map(modulMapper::toDto);
    }

    /**
     * Delete the modul by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Modul : {}", id);
        modulRepository.deleteById(id);
    }
}
