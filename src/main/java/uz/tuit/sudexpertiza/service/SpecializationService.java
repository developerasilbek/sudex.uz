package uz.tuit.sudexpertiza.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.tuit.sudexpertiza.domain.Specialization;
import uz.tuit.sudexpertiza.repository.SpecializationRepository;
import uz.tuit.sudexpertiza.service.dto.SpecializationDTO;
import uz.tuit.sudexpertiza.service.mapper.SpecializationMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Specialization}.
 */
@Service
@Transactional
public class SpecializationService {

    private final Logger log = LoggerFactory.getLogger(SpecializationService.class);

    private final SpecializationRepository specializationRepository;

    private final SpecializationMapper specializationMapper;

    public SpecializationService(SpecializationRepository specializationRepository, SpecializationMapper specializationMapper) {
        this.specializationRepository = specializationRepository;
        this.specializationMapper = specializationMapper;
    }

    /**
     * Save a specialization.
     *
     * @param specializationDTO the entity to save.
     * @return the persisted entity.
     */
    public SpecializationDTO save(SpecializationDTO specializationDTO) {
        log.debug("Request to save Specialization : {}", specializationDTO);
        Specialization specialization = specializationMapper.toEntity(specializationDTO);
        specialization = specializationRepository.save(specialization);
        return specializationMapper.toDto(specialization);
    }

    /**
     * Update a specialization.
     *
     * @param specializationDTO the entity to save.
     * @return the persisted entity.
     */
    public SpecializationDTO update(SpecializationDTO specializationDTO) {
        log.debug("Request to update Specialization : {}", specializationDTO);
        Specialization specialization = specializationMapper.toEntity(specializationDTO);
        specialization = specializationRepository.save(specialization);
        return specializationMapper.toDto(specialization);
    }

    /**
     * Partially update a specialization.
     *
     * @param specializationDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SpecializationDTO> partialUpdate(SpecializationDTO specializationDTO) {
        log.debug("Request to partially update Specialization : {}", specializationDTO);

        return specializationRepository
            .findById(specializationDTO.getId())
            .map(existingSpecialization -> {
                specializationMapper.partialUpdate(existingSpecialization, specializationDTO);

                return existingSpecialization;
            })
            .map(specializationRepository::save)
            .map(specializationMapper::toDto);
    }

    /**
     * Get all the specializations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SpecializationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Specializations");
        return specializationRepository.findAll(pageable).map(specializationMapper::toDto);
    }

    /**
     * Get one specialization by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpecializationDTO> findOne(Long id) {
        log.debug("Request to get Specialization : {}", id);
        return specializationRepository.findById(id).map(specializationMapper::toDto);
    }

    /**
     * Delete the specialization by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Specialization : {}", id);
        specializationRepository.deleteById(id);
    }
}
