package uz.tuit.sudexpertiza.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.tuit.sudexpertiza.domain.TopicTest;
import uz.tuit.sudexpertiza.repository.TopicTestRepository;
import uz.tuit.sudexpertiza.service.dto.TopicTestDTO;
import uz.tuit.sudexpertiza.service.mapper.TopicTestMapper;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TopicTest}.
 */
@Service
@Transactional
public class TopicTestService {

    private final Logger log = LoggerFactory.getLogger(TopicTestService.class);

    private final TopicTestRepository topicTestRepository;

    private final TopicTestMapper topicTestMapper;

    public TopicTestService(TopicTestRepository topicTestRepository, TopicTestMapper topicTestMapper) {
        this.topicTestRepository = topicTestRepository;
        this.topicTestMapper = topicTestMapper;
    }

    /**
     * Save a topicTest.
     *
     * @param topicTestDTO the entity to save.
     * @return the persisted entity.
     */
    public TopicTestDTO save(TopicTestDTO topicTestDTO) {
        log.debug("Request to save TopicTest : {}", topicTestDTO);
        TopicTest topicTest = topicTestMapper.toEntity(topicTestDTO);
        topicTest = topicTestRepository.save(topicTest);
        return topicTestMapper.toDto(topicTest);
    }

    /**
     * Update a topicTest.
     *
     * @param topicTestDTO the entity to save.
     * @return the persisted entity.
     */
    public TopicTestDTO update(TopicTestDTO topicTestDTO) {
        log.debug("Request to update TopicTest : {}", topicTestDTO);
        TopicTest topicTest = topicTestMapper.toEntity(topicTestDTO);
        topicTest = topicTestRepository.save(topicTest);
        return topicTestMapper.toDto(topicTest);
    }

    /**
     * Partially update a topicTest.
     *
     * @param topicTestDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<TopicTestDTO> partialUpdate(TopicTestDTO topicTestDTO) {
        log.debug("Request to partially update TopicTest : {}", topicTestDTO);

        return topicTestRepository
            .findById(topicTestDTO.getId())
            .map(existingTopicTest -> {
                topicTestMapper.partialUpdate(existingTopicTest, topicTestDTO);

                return existingTopicTest;
            })
            .map(topicTestRepository::save)
            .map(topicTestMapper::toDto);
    }

    /**
     * Get all the topicTests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TopicTestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TopicTests");
        return topicTestRepository.findAll(pageable).map(topicTestMapper::toDto);
    }

    /**
     * Get one topicTest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TopicTestDTO> findOne(Long id) {
        log.debug("Request to get TopicTest : {}", id);
        return topicTestRepository.findById(id).map(topicTestMapper::toDto);
    }

    /**
     * Delete the topicTest by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TopicTest : {}", id);
        topicTestRepository.deleteById(id);
    }
}
