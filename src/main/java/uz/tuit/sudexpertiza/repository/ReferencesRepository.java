package uz.tuit.sudexpertiza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import uz.tuit.sudexpertiza.domain.References;

/**
 * Spring Data JPA repository for the References entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReferencesRepository extends JpaRepository<References, Long>, JpaSpecificationExecutor<References> {}
