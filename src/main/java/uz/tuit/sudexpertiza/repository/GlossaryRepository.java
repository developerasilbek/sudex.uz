package uz.tuit.sudexpertiza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import uz.tuit.sudexpertiza.domain.Glossary;

/**
 * Spring Data JPA repository for the Glossary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlossaryRepository extends JpaRepository<Glossary, Long>, JpaSpecificationExecutor<Glossary> {}
