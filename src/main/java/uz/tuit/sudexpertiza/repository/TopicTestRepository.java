package uz.tuit.sudexpertiza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import uz.tuit.sudexpertiza.domain.TopicTest;

/**
 * Spring Data JPA repository for the TopicTest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TopicTestRepository extends JpaRepository<TopicTest, Long>, JpaSpecificationExecutor<TopicTest> {}
