package uz.tuit.sudexpertiza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import uz.tuit.sudexpertiza.domain.Modul;

/**
 * Spring Data JPA repository for the Modul entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModulRepository extends JpaRepository<Modul, Long>, JpaSpecificationExecutor<Modul> {}
