package uz.tuit.sudexpertiza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import uz.tuit.sudexpertiza.domain.Listener;

/**
 * Spring Data JPA repository for the Listener entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListenerRepository extends JpaRepository<Listener, Long>, JpaSpecificationExecutor<Listener> {}
