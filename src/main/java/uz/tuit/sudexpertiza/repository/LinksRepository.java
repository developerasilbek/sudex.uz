package uz.tuit.sudexpertiza.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import uz.tuit.sudexpertiza.domain.Links;

/**
 * Spring Data JPA repository for the Links entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LinksRepository extends JpaRepository<Links, Long>, JpaSpecificationExecutor<Links> {}
