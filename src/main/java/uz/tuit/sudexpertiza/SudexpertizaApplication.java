package uz.tuit.sudexpertiza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SudexpertizaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SudexpertizaApplication.class, args);
    }

}
