package uz.tuit.sudexpertiza.domain.enumeration;

/**
 * StudyType: SHARTNOMA, REJA_BUYICHA
 */
public enum StudyType {
    SHARTNOMA,
    REJA_BUYICHA
}
