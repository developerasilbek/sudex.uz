package uz.tuit.sudexpertiza.domain.enumeration;

/**
 * Centre: RSEM (Respublika Sud Expertiza markazi)
 */
public enum Centre {
    RSEM
}
