package uz.tuit.sudexpertiza.domain.enumeration;

/**
 * ConditionType: WAITING, INPROGRESS, FINISHED
 */
public enum ConditionType {
    WAITHING,
    INPROGRESS,
    FINISHED,
    UNKNOWN
}
