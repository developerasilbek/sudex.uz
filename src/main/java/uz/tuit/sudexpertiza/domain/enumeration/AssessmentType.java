package uz.tuit.sudexpertiza.domain.enumeration;

/**
 * AssessmentType: TOPIC_TEST, REFERENCES, LINKS
 */
public enum AssessmentType {
    TOPIC_TEST,
    REFERENCES,
    LINKS,
    UNKNOWN
}
