package uz.tuit.sudexpertiza.domain;

import jakarta.persistence.*;
import uz.tuit.sudexpertiza.domain.enumeration.Centre;

import java.io.Serializable;

/**
 * Specialization
 */
@Entity
@Table(name = "specialization")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Specialization implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * name
     */
    @Column(name = "name")
    private String name;

    /**
     * centre
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "centre")
    private Centre centre;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Specialization id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Specialization name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Centre getCentre() {
        return this.centre;
    }

    public Specialization centre(Centre centre) {
        this.setCentre(centre);
        return this;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Specialization)) {
            return false;
        }
        return id != null && id.equals(((Specialization) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Specialization{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", centre='" + getCentre() + "'" +
            "}";
    }
}
