package uz.tuit.sudexpertiza.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import uz.tuit.sudexpertiza.domain.enumeration.AssessmentType;

import java.io.Serializable;

/**
 * Assessment of topic
 */
@Entity
@Table(name = "assessment")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Assessment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * assessment type
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private AssessmentType type;

    @JsonIgnoreProperties(value = { "files", "modul" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Topic topic;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Assessment id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AssessmentType getType() {
        return this.type;
    }

    public Assessment type(AssessmentType type) {
        this.setType(type);
        return this;
    }

    public void setType(AssessmentType type) {
        this.type = type;
    }

    public Topic getTopic() {
        return this.topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Assessment topic(Topic topic) {
        this.setTopic(topic);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Assessment)) {
            return false;
        }
        return id != null && id.equals(((Assessment) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Assessment{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            "}";
    }
}
