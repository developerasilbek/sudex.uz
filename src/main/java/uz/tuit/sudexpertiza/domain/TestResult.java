package uz.tuit.sudexpertiza.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * Result of Test
 */
@Entity
@Table(name = "test_result")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TestResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /**
     * started at
     */
    @Column(name = "started_at")
    private Instant startedAt;

    /**
     * finished at
     */
    @Column(name = "finished_at")
    private Instant finishedAt;

    /**
     * score
     */
    @Column(name = "score")
    private Integer score;

    /**
     * number of correct answers
     */
    @Column(name = "correct_answers")
    private Integer correctAnswers;

    @JsonIgnoreProperties(value = { "assessment" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private TopicTest test;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TestResult id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getStartedAt() {
        return this.startedAt;
    }

    public TestResult startedAt(Instant startedAt) {
        this.setStartedAt(startedAt);
        return this;
    }

    public void setStartedAt(Instant startedAt) {
        this.startedAt = startedAt;
    }

    public Instant getFinishedAt() {
        return this.finishedAt;
    }

    public TestResult finishedAt(Instant finishedAt) {
        this.setFinishedAt(finishedAt);
        return this;
    }

    public void setFinishedAt(Instant finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Integer getScore() {
        return this.score;
    }

    public TestResult score(Integer score) {
        this.setScore(score);
        return this;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getCorrectAnswers() {
        return this.correctAnswers;
    }

    public TestResult correctAnswers(Integer correctAnswers) {
        this.setCorrectAnswers(correctAnswers);
        return this;
    }

    public void setCorrectAnswers(Integer correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public TopicTest getTest() {
        return this.test;
    }

    public void setTest(TopicTest topicTest) {
        this.test = topicTest;
    }

    public TestResult test(TopicTest topicTest) {
        this.setTest(topicTest);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestResult)) {
            return false;
        }
        return id != null && id.equals(((TestResult) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TestResult{" +
            "id=" + getId() +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            ", score=" + getScore() +
            ", correctAnswers=" + getCorrectAnswers() +
            "}";
    }
}
