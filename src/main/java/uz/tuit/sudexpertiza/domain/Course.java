package uz.tuit.sudexpertiza.domain;

import jakarta.persistence.*;
import uz.tuit.sudexpertiza.domain.enumeration.Centre;

import java.io.Serializable;
import java.time.Instant;

/**
 * Course
 */
@Entity
@Table(name = "course")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * name
     */
    @Column(name = "name")
    private String name;

    /**
     * centre
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "centre")
    private Centre centre;

    /**
     * language
     */
    @Column(name = "language")
    private String language;

    /**
     * teaching hour
     */
    @Column(name = "teaching_hour")
    private Integer teachingHour;

    /**
     * started at
     */
    @Column(name = "started_at")
    private Instant startedAt;

    /**
     * finished at
     */
    @Column(name = "finished_at")
    private Instant finishedAt;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Course id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Course name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Centre getCentre() {
        return this.centre;
    }

    public Course centre(Centre centre) {
        this.setCentre(centre);
        return this;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }

    public String getLanguage() {
        return this.language;
    }

    public Course language(String language) {
        this.setLanguage(language);
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getTeachingHour() {
        return this.teachingHour;
    }

    public Course teachingHour(Integer teachingHour) {
        this.setTeachingHour(teachingHour);
        return this;
    }

    public void setTeachingHour(Integer teachingHour) {
        this.teachingHour = teachingHour;
    }

    public Instant getStartedAt() {
        return this.startedAt;
    }

    public Course startedAt(Instant startedAt) {
        this.setStartedAt(startedAt);
        return this;
    }

    public void setStartedAt(Instant startedAt) {
        this.startedAt = startedAt;
    }

    public Instant getFinishedAt() {
        return this.finishedAt;
    }

    public Course finishedAt(Instant finishedAt) {
        this.setFinishedAt(finishedAt);
        return this;
    }

    public void setFinishedAt(Instant finishedAt) {
        this.finishedAt = finishedAt;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Course)) {
            return false;
        }
        return id != null && id.equals(((Course) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Course{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", centre='" + getCentre() + "'" +
            ", language='" + getLanguage() + "'" +
            ", teachingHour=" + getTeachingHour() +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            "}";
    }
}
