package uz.tuit.sudexpertiza.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Topic
 */
@Entity
@Table(name = "topic")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Topic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /**
     * name
     */
    @Column(name = "name")
    private String name;

    /**
     * priority of topic
     */
    @Column(name = "priority")
    private Integer priority;

    /**
     * hour of topic
     */
    @Column(name = "hour")
    private Integer hour;

    @OneToMany(mappedBy = "topic")
    @JsonIgnoreProperties(value = { "topic" }, allowSetters = true)
    private Set<Attachment> files = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "block" }, allowSetters = true)
    private Modul modul;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Topic id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Topic name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public Topic priority(Integer priority) {
        this.setPriority(priority);
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getHour() {
        return this.hour;
    }

    public Topic hour(Integer hour) {
        this.setHour(hour);
        return this;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Set<Attachment> getFiles() {
        return this.files;
    }

    public void setFiles(Set<Attachment> attachments) {
        if (this.files != null) {
            this.files.forEach(i -> i.setTopic(null));
        }
        if (attachments != null) {
            attachments.forEach(i -> i.setTopic(this));
        }
        this.files = attachments;
    }

    public Topic files(Set<Attachment> attachments) {
        this.setFiles(attachments);
        return this;
    }

    public Topic addFile(Attachment attachment) {
        this.files.add(attachment);
        attachment.setTopic(this);
        return this;
    }

    public Topic removeFile(Attachment attachment) {
        this.files.remove(attachment);
        attachment.setTopic(null);
        return this;
    }

    public Modul getModul() {
        return this.modul;
    }

    public void setModul(Modul modul) {
        this.modul = modul;
    }

    public Topic modul(Modul modul) {
        this.setModul(modul);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Topic)) {
            return false;
        }
        return id != null && id.equals(((Topic) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Topic{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", priority=" + getPriority() +
            ", hour=" + getHour() +
            "}";
    }
}
