package uz.tuit.sudexpertiza.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.*;
import uz.tuit.sudexpertiza.domain.enumeration.QuestionType;

import java.io.Serializable;

/**
 * Question of test
 */
@Entity
@Table(name = "question")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * name of question
     */
    @Column(name = "name")
    private String name;

    /**
     * required score
     */
    @Column(name = "required_score")
    private Integer requiredScore;

    /**
     * question type
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private QuestionType type;

    @ManyToOne
    @JsonIgnoreProperties(value = { "assessment" }, allowSetters = true)
    private TopicTest test;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Question id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Question name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRequiredScore() {
        return this.requiredScore;
    }

    public Question requiredScore(Integer requiredScore) {
        this.setRequiredScore(requiredScore);
        return this;
    }

    public void setRequiredScore(Integer requiredScore) {
        this.requiredScore = requiredScore;
    }

    public QuestionType getType() {
        return this.type;
    }

    public Question type(QuestionType type) {
        this.setType(type);
        return this;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public TopicTest getTest() {
        return this.test;
    }

    public void setTest(TopicTest topicTest) {
        this.test = topicTest;
    }

    public Question test(TopicTest topicTest) {
        this.setTest(topicTest);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Question)) {
            return false;
        }
        return id != null && id.equals(((Question) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Question{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", requiredScore=" + getRequiredScore() +
            ", type='" + getType() + "'" +
            "}";
    }
}
