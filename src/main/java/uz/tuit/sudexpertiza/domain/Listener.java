package uz.tuit.sudexpertiza.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import jakarta.persistence.*;
import uz.tuit.sudexpertiza.domain.enumeration.ConditionType;
import uz.tuit.sudexpertiza.domain.enumeration.StudyType;

import java.io.Serializable;

/**
 * Listener
 */
@Entity
@Table(name = "listener")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Listener implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * phone number
     */
    @NotNull
    @Size(max = 15)
    @Column(name = "phone", length = 15, nullable = false, unique = true)
    private String phone;

    /**
     * region
     */
    @Column(name = "region")
    private String region;

    /**
     * workplace
     */
    @Column(name = "workplace")
    private String workplace;

    /**
     * Date of birth
     */
    @Column(name = "date_of_birth")
    private String dateOfBirth;

    /**
     * condition of listener
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "condition")
    private ConditionType condition;

    /**
     * study type
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private StudyType type;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @JsonIgnoreProperties(value = { "topic" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Attachment contract;

    @ManyToOne
    private Course course;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Listener id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return this.phone;
    }

    public Listener phone(String phone) {
        this.setPhone(phone);
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegion() {
        return this.region;
    }

    public Listener region(String region) {
        this.setRegion(region);
        return this;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getWorkplace() {
        return this.workplace;
    }

    public Listener workplace(String workplace) {
        this.setWorkplace(workplace);
        return this;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getDateOfBirth() {
        return this.dateOfBirth;
    }

    public Listener dateOfBirth(String dateOfBirth) {
        this.setDateOfBirth(dateOfBirth);
        return this;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public ConditionType getCondition() {
        return this.condition;
    }

    public Listener condition(ConditionType condition) {
        this.setCondition(condition);
        return this;
    }

    public void setCondition(ConditionType condition) {
        this.condition = condition;
    }

    public StudyType getType() {
        return this.type;
    }

    public Listener type(StudyType type) {
        this.setType(type);
        return this;
    }

    public void setType(StudyType type) {
        this.type = type;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Listener user(User user) {
        this.setUser(user);
        return this;
    }

    public Attachment getContract() {
        return this.contract;
    }

    public void setContract(Attachment attachment) {
        this.contract = attachment;
    }

    public Listener contract(Attachment attachment) {
        this.setContract(attachment);
        return this;
    }

    public Course getCourse() {
        return this.course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Listener course(Course course) {
        this.setCourse(course);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Listener)) {
            return false;
        }
        return id != null && id.equals(((Listener) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Listener{" +
            "id=" + getId() +
            ", phone='" + getPhone() + "'" +
            ", region='" + getRegion() + "'" +
            ", workplace='" + getWorkplace() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", condition='" + getCondition() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
